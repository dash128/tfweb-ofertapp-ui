import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import FBuscarTrabajo from './components/Freelancer/FreelancerBuscarTrabajo.vue'
import FVerPostulacion from './components/Freelancer/FreelancerVerPostulacion.vue'
import FVerTrabajo from './components/Freelancer/FreelancerVerTrabajo.vue'

import ECrearTrabajo from './components/Empresa/EmpresaCrearTrabajo.vue'
import EEditarTrabajo from './components/Empresa/EmpresaEditarTrabajo.vue'
import EVerSolicitud from './components/Empresa/EmpresaVerSolicitud.vue'


Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/empresa',
      name: 'empresa',
      component: () => import(/* webpackChunkName: "about" */ './views/ViewAdmin.vue')
    },
    {
      path: '/freelancer/buscador',
      name: 'freelancerBuscarTrabajo',
      component: FBuscarTrabajo
    },{
      path: '/freelancer/mispostulaciones',
      name: 'freelancerVerPostulacion',
      component: FVerPostulacion
    },{
      path: '/freelancer/mistrabajos',
      name: 'freelancerVerTrabajo',
      component: FVerTrabajo
    },

    {
      path: '/empresa/creartrabajo',
      name: 'empresaCrearTrabajo',
      component: ECrearTrabajo
    },
    {
      path: '/empresa/editartrabajo',
      name: 'empresaEditarTrabajo',
      component: EEditarTrabajo
    },
    {
      path: '/empresa/solicitudes',
      name: 'empresaVerSolicitud',
      component: EVerSolicitud
    }

  ]
})
