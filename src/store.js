import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    menuUser:false,
    menuAdmin:false,
    drawer:false,
    userActual:null
  },
  mutations: {
    handleMenuUser(state){
      state.menuUser = !state.menuUser
      console.log("change menuUser to:",state.menuUser);
    },
    handleMenuAdmin(state){
      state.menuAdmin = !state.menuAdmin
      console.log("change menuAdmin to:",state.menuAdmin);
    },
    setUserActual(state, userActual){
      state.userActual = userActual
      console.log("user:",state.userActual);
    }
  },
  actions: {

  }
})
